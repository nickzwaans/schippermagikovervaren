## About this project
Please note this is the first time doing a laravel project from scratch so there will probably be a lot of default overhead in the codebase.

Get the project up and running via: 
```bash
$ composer install
$ ./vendor/bin/sail up -d
$ ./vendor/bin/sail php artisan migrate
$ ./vendor/bin/sail php artisan key:generate
$ ./vendor/bin/sail php artisan serve
```

### Building the frontend
```bash
$ npm install
$ npm run build
```
