require('./bootstrap');
$(document).ready(function () {
    $('[data-form-label]').each(function () {
        const formLabel = new FormLabel(this);
        formLabel.init();
    });
});

class FormLabel {
    constructor(element) {
        this.form = $(element);

        var domain = window.location.protocol + '//' + window.location.hostname +
            (window.location.port ? ':' + window.location.port : '')
        ;

        this.urls = {
            donate: this.form.attr('action'),
            successUrl: domain + '/thanks.html',
            error: domain + '/error.html',
        };

        this.loader = $('.form__loader-content', this.form);


    }

    init() {
        $('.form__loader-close', this.form).on('click', this.onClickClose.bind(this));

        this.form.on('submit', this.onSubmit.bind(this));
    }

    onClickClose(e) {
        e.preventDefault();
        this.form.removeClass('is-submitting');
    }

    onSubmit(e) {
        e.preventDefault();
        $('.site__content').animate({scrollTop: 0}, 500);
        this.form.addClass('is-submitting');

        var data = this.getFormData();
        var config = {
            type: 'POST',
            url: this.urls.donate,
            data: JSON.stringify(data),
            dataType: 'json',
            contentType: 'application/json',
        };

        console.log('onSubmit - ajax', config);

        $.ajax(config)
            .done((data) => {
                console.log('Ajax SUCCESS', data);
                var redirectUrl = data.redirectUrl;
                if (redirectUrl) {
                    window.location.href = redirectUrl;
                } else {
                    this.loader.html('<p>Success!</p>');
                }
            })
            .fail((jqXHR, status, error) => {
                console.log('Ajax ERROR', status, error);
                var html = '<p>Error :(</p>';

                if (jqXHR.status === 400) {
                    var response = JSON.parse(jqXHR.responseText);
                    var info = response.info.map(line => line.message);

                    html = '<h2>Validation errors</h2><ul style="text-align: left;"><li>' + info.join('</li><li>') + '</li></ul>';
                }

                this.loader.html(html);

            })
        ;
    }


    getFormData() {
        var data = this.formToJSON();

        if (data['recipientPostcode']) {
            data['recipientPostcode'] = data['recipientPostcode'].replace(/\s/g, '');
        }

        if (data['senderPostcode']) {
            data['senderPostcode'] = data['senderPostcode'].replace(/\s/g, '');
        }

        return data;
    }

    formToJSON() {
        var fields = this.form.serializeArray();
        var json = {};

        for (var i = 0; i < fields.length; i++) {
            var name = fields[i]['name'];
            var value = fields[i]['value'];

            // If first occurrence - Add to json
            if (typeof json[name] === 'undefined') {
                json[name] = value;
            }
            // If second occurrence - Convert to array
            else if (typeof json[name] === 'string') {
                json[name] = [json[name], value];
            }
            // If third occurrence (or more) - Add to array
            else {
                json[name].push(value);
            }
        }

        return json;
    }
}

