<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>pdf</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<p>{{ $recipient_first_name }}</p>
<p>{{ $recipient_last_name }}</p>
<p>{{ $recipient_street }}</p>
<p>{{ $recipient_city }}</p>
<p>{{ $recipient_country }}</p>
@php
    $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
@endphp
{!! $generator->getBarcode($barcode, $generator::TYPE_CODE_128) !!}
<p>{{ $barcode }}</p>
</body>
</html>
