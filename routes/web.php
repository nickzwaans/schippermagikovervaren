<?php

use App\Http\Controllers\PdfController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('form', ['appUrl' => env('APP_URL', 'http://localhost')]);
});
Route::get('pdf/{id}', function ($id) {
    return PdfController::get($id);
});

Route::resource('label', \App\Http\Controllers\LabelController::class);

