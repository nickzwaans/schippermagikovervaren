<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labels', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('barcode');
            $table->string('recipient_first_name');
            $table->string('recipient_last_name');
            $table->string('recipient_city');
            $table->string('recipient_street');
            $table->string('recipient_postcode');
            $table->string('recipient_country');
            $table->string('sender_first_name');
            $table->string('sender_last_name');
            $table->string('sender_city');
            $table->string('sender_street');
            $table->string('sender_postcode');
            $table->string('sender_country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labels');
    }
}
