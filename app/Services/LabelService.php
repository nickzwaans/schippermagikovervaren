<?php

namespace App\Services;

use App\Dtos\LabelData;
use App\Models\Label;
use App\Repositories\LabelRepository;
use Illuminate\Support\Facades\DB;
use const Cerbero\Dto\MUTABLE;
use const Cerbero\Dto\PARTIAL;

class LabelService
{

    /**
     * @var $labelRepository
     */
    protected $labelRepository;

    /**
     * LabelService constructor.
     *
     * @param \App\Repositories\LabelRepository $labelRepository
     */
    public function __construct(LabelRepository $labelRepository)
    {
        $this->labelRepository = $labelRepository;
    }

    /**
     * Create a label.
     *
     * @param \App\Dtos\LabelData $dto
     *
     * @return mixed
     */
    public function create(LabelData $dto)
    {
        $dto->barcode = BarcodeService::generate($this->getLastInsertedId());

        return $this->labelRepository->save($dto);
    }

    /**
     * Find a label by its id.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function findOne(int $id)
    {
        return $this->labelRepository->find($id);
    }

    /**
     * Update a label.
     *
     * @param int $id
     * @param \App\Dtos\LabelData $dto
     */
    public function update(int $id, LabelData $dto)
    {
        $label = $this->labelRepository->find($id);
        $label->fill($dto->toArray());

        Label::update($label);
    }

    public function delete(int $id)
    {

    }


    public function findMany()
    {

    }

    /**
     * Get the last inserted id.
     *
     * @return int|mixed
     */
    private function getLastInsertedId()
    {
        $latest = DB::table('labels')->latest()->first();

        return $latest ? $latest->id : 0;
    }
}
