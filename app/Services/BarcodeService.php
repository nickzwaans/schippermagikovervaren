<?php

namespace App\Services;

class BarcodeService
{
    const PREFIX = 'MP';

    /**
     * @param int $id
     *
     * @return string
     */
    public static function generate(int $id): string
    {
        return self::PREFIX . str_pad($id, 8, '0', STR_PAD_LEFT);
    }
}
