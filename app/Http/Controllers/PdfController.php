<?php

namespace App\Http\Controllers;

use App\Models\Label;
use Illuminate\Support\Facades\App;

class PdfController
{
    /**
     * @param int $id
     *
     * @return string
     */
    public static function get(int $id): string
    {
        // todo make label into dto specifically for the pdf.
        $data = Label::findOrFail($id);

        // cant get this to work.
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf', $data);

        return view('pdf', $data);
    }
}
