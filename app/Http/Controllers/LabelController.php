<?php

namespace App\Http\Controllers;

use App\Dtos\LabelData;
use App\Services\LabelService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use const Cerbero\Dto\MUTABLE;
use const Cerbero\Dto\PARTIAL;

class LabelController extends Controller
{
    /**
     * The label service implementation.
     *
     * @var \App\Services\LabelService
     */
    protected $labelService;

    protected $pdfService;


    /**
     * Create a new controller instance.
     *
     * @param \App\Services\LabelService $labelService
     */
    public function __construct(
        LabelService $labelService
    ) {
        $this->labelService = $labelService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $labelDto = LabelData::fromRequest($request, PARTIAL);
        $labelDto = new LabelData($labelDto->toArray(), MUTABLE);

        $result = ['status' => Response::HTTP_CREATED];

        // Create label.
        try {
            $label = $this->labelService->create($labelDto);
            $result['data'] = $label;
            $result['redirectUrl'] = env('APP_URL') . '/pdf/' . $label->id;
        } catch (\Exception $exception) {
            $result = [
                'status' => Response::HTTP_BAD_REQUEST,
                'error' => $exception->getMessage(),
            ];
        }

        return response()->json($result, $result['status']);
    }
}
