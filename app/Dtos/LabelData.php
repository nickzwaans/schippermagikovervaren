<?php

namespace App\Dtos;

use Carbon\Carbon;
use Cerbero\LaravelDto\Dto;

use const Cerbero\Dto\PARTIAL;
use const Cerbero\Dto\IGNORE_UNKNOWN_PROPERTIES;

/**
 * The data transfer object for the Label model.
 *
 * @property int $id
 * @property Carbon|null $createdAt
 * @property Carbon|null $updatedAt
 * @property string $barcode
 * @property string $recipientFirstName
 * @property string $recipientLastName
 * @property string $recipientCity
 * @property string $recipientStreet
 * @property string $recipientPostcode
 * @property string $recipientCountry
 * @property string $senderFirstName
 * @property string $senderLastName
 * @property string $senderCity
 * @property string $senderStreet
 * @property string $senderPostcode
 * @property string $senderCountry
 */
class LabelData extends Dto
{
    /**
     * The default flags.
     *
     * @var int
     */
    protected static $defaultFlags = PARTIAL | IGNORE_UNKNOWN_PROPERTIES;
}
