<?php

namespace App\Repositories;

use App\Dtos\LabelData;
use App\Models\Label;

class LabelRepository
{

    /**
     * @var \App\Models\Label
     */
    protected $label;

    /**
     * LabelRepository constructor.
     *
     * @param \App\Models\Label $label
     */
    public function __construct(Label $label)
    {
        $this->label = $label;
    }

    /**
     * @param \App\Dtos\LabelData $dto
     *
     * @return mixed
     */
    public function save(LabelData $dto)
    {
        $label = new $this->label;

        $label->barcode = $dto->barcode;
        $label->recipient_first_name = $dto->recipientFirstName;
        $label->recipient_last_name = $dto->recipientLastName;
        $label->recipient_city = $dto->recipientCity;
        $label->recipient_street = $dto->recipientStreet;
        $label->recipient_postcode = $dto->recipientPostcode;
        $label->recipient_country = $dto->recipientCountry;
        $label->sender_first_name = $dto->senderFirstName;
        $label->sender_last_name = $dto->senderLastName;
        $label->sender_city = $dto->senderCity;
        $label->sender_street = $dto->senderStreet;
        $label->sender_postcode = $dto->senderPostcode;
        $label->sender_country = $dto->senderCountry;

        $label->save();

        return $label->fresh();
    }

    public function find($id)
    {
        return $this->label->find($id);
    }
}
