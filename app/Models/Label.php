<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    use HasFactory;

    protected $table = 'labels';

    protected $fillable = [
        'barcode',
        'recipient_first_name',
        'recipient_last_name',
        'recipient_city',
        'recipient_street',
        'recipient_postcode',
        'recipient_country',
        'sender_first_name',
        'sender_last_name',
        'sender_city',
        'sender_street',
        'sender_postcode',
        'sender_country',
    ];
}
